
import 'react-native';
import React from 'react';
import MainPage from '../js/pages/MainPage';

import renderer from 'react-test-renderer';

it('renders correctly', () => {
    const tree = renderer.create(
      <MainPage />
      ).toJSON();
    expect(tree).toMatchSnapshot();
});