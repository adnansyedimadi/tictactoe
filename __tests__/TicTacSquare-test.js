
import 'react-native';
import React from 'react';
import TicTacSquare from '../js/components/TicTacSquare';
import { shallow } from 'enzyme';

import renderer from 'react-test-renderer';

const Props = {
	theme: {
        primaryLight: 'white',
        secondaryLight: 'white',
    },
    draw: 'o'
}

it('renders correctly', () => {
    const tree = renderer.create(
      <TicTacSquare {...Props}/>
      ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('renders as expected', () => {
    const wrapper = shallow(
      <TicTacSquare {...Props} />
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.setProps({ draw: 'x' });
    expect(wrapper).toMatchSnapshot();
});