import 'react-native';
import GameLogic from '../js/utils/GameLogic';

describe('Simple Functions Tests', function() {
    test('Max Moves Test', async () => {
        expect(GameLogic.getMaxMovesToTie(3)).toEqual(9);
        expect(GameLogic.getMaxMovesToTie(4)).toEqual(16);
        expect(GameLogic.getMaxMovesToTie(5)).toEqual(25);
    });

    test('Min Moves Test', async () => {
        expect(GameLogic.getMinMovesToWin(3)).toEqual(5);
        expect(GameLogic.getMinMovesToWin(4)).toEqual(7);
        expect(GameLogic.getMinMovesToWin(5)).toEqual(9);
    });
    test('Create Board Test', async () => {
        let threeBoard = [[null,null,null],[null,null,null],[null,null,null]];
        let fourBoard = [[null,null,null,null],[null,null,null,null],[null,null,null,null],[null,null,null,null]];
        let fiveBoard = [[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null],[null,null,null,null,null]];
        expect(GameLogic.createBoard(3)).toEqual(threeBoard);
        expect(GameLogic.createBoard(4)).toEqual(fourBoard);
        expect(GameLogic.createBoard(5)).toEqual(fiveBoard);
    });
});

describe('Win Logic Tests', function() {
    describe('3x3 Win Logic', function() {
        test('Row Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 0;
            let threeBoard = [[keySelected,keySelected,keySelected],[null,null,null],[null,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, threeBoard, selectedRow, selectedCol)).toEqual([[0,0], [0,1], [0,2]]);
        });

        test('Col Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 0;
            let threeBoard = [[keySelected,null,null],[keySelected,null,null],[keySelected,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, threeBoard, selectedRow, selectedCol)).toEqual([[0,0], [1,0], [2,0]]);
        });

        test('Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 0;
            let threeBoard = [[keySelected,null,null],[null,keySelected,null],[null,null,keySelected]];
            expect(GameLogic.checkAndGetWinArray(keySelected, threeBoard, selectedRow, selectedCol)).toEqual([[0,0], [1,1], [2,2]]);
        });

        test('Anti-Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 2;
            let selectedCol = 0;
            let threeBoard = [[null,null,keySelected],[null,keySelected,null],[keySelected,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, threeBoard, selectedRow, selectedCol)).toEqual([[0,2], [1,1], [2,0]]);
        });

    });

    describe('4x4 Win Logic', function() {
        test('Row Match', async () => {
            let keySelected = 'x';
            let selectedRow = 1;
            let selectedCol = 1;
            let fourBoard = [[null,null,null,null],[keySelected,keySelected,keySelected,keySelected],[null,null,null,null],[null,null,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fourBoard, selectedRow, selectedCol)).toEqual([[1,0],[1,1],[1,2],[1,3]]);
        });

        test('Col Match', async () => {
            let keySelected = 'x';
            let selectedRow = 1;
            let selectedCol = 1;
            let fourBoard = [[null,keySelected,null,null],[null,keySelected,null,null],[null,keySelected,null,null],[null,keySelected,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fourBoard, selectedRow, selectedCol)).toEqual([[0,1],[1,1],[2,1],[3,1]]);
        });

        test('Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 0;
            let fourBoard = [[keySelected,null,null,null],[null,keySelected,null,null],[null,null,keySelected,null],[null,null,null,keySelected]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fourBoard, selectedRow, selectedCol)).toEqual([[0,0],[1,1],[2,2],[3,3]]);
        });

        test('Anti-Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 3;
            let selectedCol = 0;
            let fourBoard = [[null,null,null,keySelected],[null,null,keySelected,null],[null,keySelected,null,null],[keySelected,null,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fourBoard, selectedRow, selectedCol)).toEqual([[0,3],[1,2],[2,1],[3,0]]);
        });

    });

    describe('5x5 Win Logic', function() {
        test('Row Match', async () => {
            let keySelected = 'x';
            let selectedRow = 2;
            let selectedCol = 2;
            let fiveBoard = [[null,null,null,null,null],[null,null,null,null,null],[keySelected,keySelected,keySelected,keySelected,keySelected],[null,null,null,null,null],[null,null,null,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fiveBoard, selectedRow, selectedCol)).toEqual([[2,0],[2,1],[2,2],[2,3],[2,4]]);
        });
        test('Col Match', async () => {
            let keySelected = 'x';
            let selectedRow = 2;
            let selectedCol = 2;
            let fiveBoard = [[null,null,keySelected,null,null],[null,null,keySelected,null,null],[null,null,keySelected,null,null],[null,null,keySelected,null,null],[null,null,keySelected,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fiveBoard, selectedRow, selectedCol)).toEqual([[0,2],[1,2],[2,2],[3,2],[4,2]]);
        });

        test('Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 0;
            let fiveBoard = [[keySelected,null,null,null,null],[null,keySelected,null,null,null],[null,null,keySelected,null,null],[null,null,null,keySelected,null],[null,null,null,null,keySelected]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fiveBoard, selectedRow, selectedCol)).toEqual([[0,0],[1,1],[2,2],[3,3],[4,4]]);
        });

        test('Anti-Diagonal Match', async () => {
            let keySelected = 'x';
            let selectedRow = 0;
            let selectedCol = 4;
            let fiveBoard = [[null,null,null,null,keySelected],[null,null,null,keySelected,null],[null,null,keySelected,null,null],[null,keySelected,null,null,null],[keySelected,null,null,null,null]];
            expect(GameLogic.checkAndGetWinArray(keySelected, fiveBoard, selectedRow, selectedCol)).toEqual([[0,4],[1,3],[2,2],[3,1],[4,0]]);
        });

    });

});