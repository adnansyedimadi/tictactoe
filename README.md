Welcome to Tic Tac Toe

Features:
* 2 Players
* Keep Track of Score
* 3x3, 4x4, 5x5 grid options
* Dark and Light Theme 

The challenges I put for myself: 
* Make the game work regardless of what size grid was choosen. Therefore the TicTacToe grid resizes itself to fit regardless of the size choosen and regardless of the device screen size.
* Make win condition algorithm to be as effeicent as possible. Checking win conditions of the board is linear time. O(n) in a (n X n) grid.  
* Have the ability to toggle between light and dark theme.

This app was made with react native best practices in mind to the best of my ability 

I hope you enjoy the app

