/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './js/pages/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
