
export default {
    primary: "#cccccc",
    primaryLight: "#6bd5b2aa",
    secondary: "#6bd5b2aa",
    secondaryLight: "#1f0c6caa",
    textColorPrimary: "#000000",
    backgroundColor: "#ffffff"
};
