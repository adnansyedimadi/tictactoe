
export default {
    primary: "#212121",
    primaryLight: "#BB86FCaa",
    secondary: "#BB86FCff",
    secondaryLight: "#03DAC6aa",
    textColorPrimary: "#ffffff",
    backgroundColor: "#121212"
};