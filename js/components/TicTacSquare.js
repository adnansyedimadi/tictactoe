import React, {Component} from 'react';
import {
  View,
  Dimensions,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

const window = Dimensions.get('window');

class TicTacSquare extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles(this.props).square}>
          <Text style={styles(this.props).sqaureText}>{this.props.draw == null ? "" : this.props.draw.toUpperCase()}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = (props) => StyleSheet.create({
  square: {
    width:  (window.width / props.boardSize) * 0.85, 
    height: (window.width / props.boardSize)* 0.85, 
    alignItems:'center', 
    justifyContent:'center', 
    backgroundColor: props.winSquare != true ? props.theme.primaryLight : props.theme.secondaryLight,
    borderRadius: 10,
    backfaceVisibility: 'hidden'
  },
  sqaureText:{
    fontSize: 50,
    color: props.theme.textColorPrimary,
    opacity: 1
  }
});


export default TicTacSquare;
  