import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions
} from 'react-native';

import TicTacSquare from "../components/TicTacSquare";
import GameLogic from "../utils/GameLogic";
import LightTheme from "../themes/LightTheme";
import DarkTheme from "../themes/DarkTheme";

const window = Dimensions.get('window');

class MainPage extends Component {

    static WIN = 1;
    static ONGOING = 0
    static TIE = -1;

    constructor(props) {
        super(props);
        this.state={
            boardSize: 3,
            boardMinMovesToWin: 5,
            boardMaxMovesToTie: 9,
            board: [[null,null,null], [null,null,null], [null,null,null]],
            boardColored: [[null,null,null], [null,null,null], [null,null,null]],

            countTurn: 0,
            gameState: 0, //-1 Tie, 0 Ongoing Game, 1 Win

            currentToken: 'x',

            firstToken: 'x',
            secondToken: 'o',
            countFirstTokenWins: 0,
            countSecondTokenWins: 0,
            
            theme: LightTheme,
        }
    }

    // componentDidMount(){
    //     this.setUpBoard(this.state.boardSize);
    // }

    setUpBoard(size){
        this.setState({
            boardSize: size,
            boardMinMovesToWin: GameLogic.getMinMovesToWin(size),
            boardMaxMovesToTie: GameLogic.getMaxMovesToTie(size),
            board: GameLogic.createBoard(size),
            boardColored: GameLogic.createBoard(size),
            currentToken: this.state.firstToken,
            countTurn: 0,
            gameState: 0,
        });
    }

    changeState(x,y){
        var board = this.state.board;
        let nextTurnToken = this.state.currentToken == this.state.firstToken ? this.state.secondToken : this.state.firstToken;
        let countTurn = this.state.countTurn + 1;

        //Check to make sure the a square isnt over written and game isn't in the finished state
        console.log("GameState: ", this.state.gameState);
        console.log("onGoing: ", MainPage.ONGOING);
        if(board[x][y] != null || this.state.gameState != MainPage.ONGOING){
            return;
        }

        board[x][y] = this.state.currentToken;

        //Only start checking if minimum condition are met
        if(countTurn >= this.state.boardMinMovesToWin){

            let winArray = GameLogic.checkAndGetWinArray(this.state.currentToken, board, x, y);
            if(winArray != null){
                this.handleWin(winArray);
                nextTurnToken = this.state.currentToken;
            }
            else if(winArray == null && countTurn == this.state.boardMaxMovesToTie){
                this.handleTie();
            }

        }

        this.setState({
            board: board,
            currentToken: nextTurnToken,
            countTurn: countTurn
        });
    }


    handleWin(winArray){
        let boardColored = this.state.boardColored;
        var countFirstTokenWins = this.state.countFirstTokenWins;
        var countSecondTokenWins = this.state.countSecondTokenWins;

        if(this.state.currentToken == this.state.firstToken){
            countFirstTokenWins++;
        } else{
            countSecondTokenWins++;
        }

        for(var key in winArray){
            let x = winArray[key][0];
            let y = winArray[key][1];
            boardColored[x][y] = true;
        }

        this.setState({
            gameState: MainPage.WIN,
            boardColored: boardColored,
            countFirstTokenWins: countFirstTokenWins,
            countSecondTokenWins: countSecondTokenWins
        })
    }

    resetScore(){
        this.setState({
            countFirstTokenWins: 0,
            countSecondTokenWins: 0
        });
    }

    handleTie(){
        this.setState({
            gameState: MainPage.TIE
        });
    }

    showGameState(){
        if(this.state.gameState == MainPage.WIN){
            return "Winner is: " + this.state.currentToken.toUpperCase();
        }else if(this.state.gameState == MainPage.TIE){
            return "This Game is a Tie";
        }else{
            return "Current Turn: " + this.state.currentToken.toUpperCase();
        }
    }

    toggleTheme(){
        this.setState({
            theme: this.state.theme == LightTheme ? DarkTheme : LightTheme
        })
    }

    buildBoard(){
        return(
            <View>
                {this.state.board.map((col, i) => {
                    return(this.buildRow(i));
                })}
            </View>
        )
    }

    buildRow(i){
        return (
            <View key={i} style={{flexDirection:'row', padding: 2}}>
                {this.state.board.map((row, j) => {
                    return (
                        <View key={i,j} style={{padding: 2}}>
                            <TicTacSquare key={i,j} boardSize={this.state.boardSize} winSquare={this.state.boardColored[i][j]} theme={this.state.theme} draw={this.state.board[i][j]} onPress={()=>this.changeState(i,j)} />
                        </View>
                    )
                })}
            </View>
        )
    }


    render() {
        return (
            <>
            <StatusBar barStyle= {this.state.theme == LightTheme ? "dark-content" : "light-content"} />
            <SafeAreaView style={styles(this.state).scrollView}>
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles(this.state).scrollView}>
                <View style={{height: 100, alignItems:'center', justifyContent:'center'}}>
                    <Text style={styles(this.state).headerText}>Tic Tac Toe</Text>
                    <View style={{flexDirection:'row', width: window.width * 0.9, justifyContent: 'space-around',}}>
                        <Text style={styles(this.state).headerWinText}>{this.state.firstToken.toUpperCase()} Win: {this.state.countFirstTokenWins}</Text>
                        <Text style={styles(this.state).headerWinText}>{this.state.secondToken.toUpperCase()} Win: {this.state.countSecondTokenWins}</Text>
                    </View>
                </View>
                <View style={styles(this.state).body}>
                    <View style={{justifyContent:'center', alignItems:'center', paddingBottom: 20, paddingTop: 20}}>
                        <Text style={styles(this.state).gameStatusText}>{this.showGameState()}</Text>
                    </View>
                    
                    <View style={{width: window.width * 0.9, justifyContent: 'center', alignItems: 'center'}}>
                        {this.buildBoard()}
                    </View>
                    <View style={styles(this.state).firstButtonRowView}>
                        <TouchableOpacity onPress={()=>this.resetScore()}>
                            <View style={styles(this.state).firstButtonRow}>
                                <Text style={styles(this.state).buttonText}>Reset Score</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setUpBoard(this.state.boardSize)}>
                            <View style={styles(this.state).firstButtonRow}>
                                <Text style={styles(this.state).buttonText}>New Game</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={styles(this.state).secondButtonRowView}>
                        <TouchableOpacity onPress={()=>this.setUpBoard(3)}>
                            <View style={[styles(this.state).secondButtonRow,{backgroundColor: this.state.boardSize == 3 ? this.state.theme.secondary: this.state.theme.primary}]}>
                                <Text style={styles(this.state).buttonText}>3x3</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setUpBoard(4)}>
                            <View style={[styles(this.state).secondButtonRow,{backgroundColor: this.state.boardSize == 4 ? this.state.theme.secondary: this.state.theme.primary}]}>
                                <Text style={styles(this.state).buttonText}>4x4</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setUpBoard(5)}>
                            <View style={[styles(this.state).secondButtonRow,{backgroundColor: this.state.boardSize == 5 ? this.state.theme.secondary: this.state.theme.primary}]}>
                                <Text style={styles(this.state).buttonText}>5x5</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={styles(this.state).thirdButtonRowView}>
                        <TouchableOpacity onPress={()=>this.toggleTheme()}>
                            <View style={styles(this.state).thirdButtonRow}>
                                <Text style={styles(this.state).buttonText}>Toggle Theme</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    
                </View>
            </ScrollView>
            </SafeAreaView>
        </>
        );
    }
}

const styles = (state) => StyleSheet.create({
    scrollView: {
      backgroundColor: state.theme.backgroundColor,
      height: window.height
    },
    body: {
      backgroundColor: state.theme.backgroundColor,
      justifyContent:'center',
      alignItems:"center"
    },
    viewBackground:{
        backgroundColor: state.theme.backgroundColor,
    },
    headerText:{
        fontSize:50, 
        fontWeight:'bold',
        color: state.theme.textColorPrimary
    },
    headerWinText:{
        fontSize: 20, 
        fontWeight: 'bold',
        color: state.theme.textColorPrimary
    },
    gameStatusText:{
        fontSize:30, 
        fontWeight:'bold',
        color: state.theme.textColorPrimary
    },
    buttonText:{
        color: state.theme.textColorPrimary
    },
    firstButtonRowView:{
        paddingTop: 20, 
        flexDirection:"row", 
        justifyContent: 'space-around', 
        width: window.width * 0.7
    },
    firstButtonRow:{
        width: window.width * 0.32, 
        height: 30, 
        backgroundColor: state.theme.primary, 
        borderRadius:20, 
        alignItems:'center', 
        justifyContent:'center'
    },
    secondButtonRowView:{
        paddingTop: 20, 
        flexDirection:"row", 
        justifyContent: 'space-around', 
        width: window.width * 0.8
    },
    secondButtonRow:{
        width: window.width * 0.22, 
        height: 30, 
        backgroundColor: state.theme.primary, 
        borderRadius:20, 
        alignItems:'center', 
        justifyContent:'center'
    },
    thirdButtonRowView:{
        paddingTop: 20, 
        flexDirection:"row", 
        justifyContent: 'space-around',
        width: window.width * 0.9,
        paddingBottom: 20
    },
    thirdButtonRow:{
        width: window.width * 0.85, 
        height: 30, 
        backgroundColor: state.theme.primary, 
        borderRadius:20, 
        alignItems:'center', 
        justifyContent:'center'
    },
  });

export default MainPage;
  