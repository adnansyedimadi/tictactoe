/**
 * Tic Tac Toe
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import MainPage from "./MainPage";

const App: () => React$Node = () => {
  return (
    <MainPage />
  );
};

export default App;
