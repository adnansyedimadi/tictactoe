class GameLogic {

    static checkAndGetWinArray(currentToken, board, x, y){
        let boardSize = board.length; //3

        var colArray = [];
        var rowArray = [];
        var diagArray = [];
        var antiDiagArray = [];

        for(var i = 0; i < boardSize; i++){
            if(board[x][i] == currentToken){//Up and Down
                colArray.push([x,i]);
            }
            if(board[i][y] == currentToken){//Left and Right
                rowArray.push([i,y]);
            }
            if(board[i][i] == currentToken){//Diagonal down from 0,0
                diagArray.push([i,i]);
            }
            if(board[i][boardSize-i-1] == currentToken){//Diagonal up from 0,2
                antiDiagArray.push([i,boardSize-i-1]);
            }
        }

        if(colArray.length == boardSize){
            return colArray;
        }
        if(rowArray.length == boardSize){
            return rowArray;
        }
        if(diagArray.length == boardSize){
            return diagArray;
        }
        if(antiDiagArray.length == boardSize){
            return antiDiagArray;
        }

        return null;
    }
   
    static getMinMovesToWin(size){
        return (size * 2) -1;
    }

    static getMaxMovesToTie(size){
        return size * size;
    }

    static createBoard(size){
        var board = new Array(size);
        for(var i = 0; i < size; i++){
            let boardCol = new Array(size);
            for(var j = 0; j < size; j++){
                boardCol[j] = null;
            }
            board[i] = boardCol;
        }
        return board;
    }

}


export default GameLogic;